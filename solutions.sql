-- MATTHEW A. MANALILI CSIT343 - G2 
2. Given the following data, provide the answer to the following:
-- a. List the books Authored by Marjorie Green.
    --Answer:
        -- The Busy Executive's Database Guide
        -- You Can Combat Computer Stress!

-- b. List the books Authored by Michael O'Leary.
    --Answer:
        -- Cooking with Computers

-- c. Write the author/s of "The Busy Executives Database Guide".
    --Answer:
        -- Marjorie Green
        -- Bennet Abraham

-- d. Identify the publisher of "But Is It User Friendly?".
    --Answer:
        -- Algodata InfoSystems

-- e. List the books published by Algodata Infosystems.
    --Answer:
        -- The Busy Executive's Database Guide
        -- Cooking with Computers
        -- Straight Talk About Computers
        -- But is It User Friendly?
        -- Secrets of Silicon Valley
        -- Net Etiquette
    
3. Create SQL Syntax and Queries to create a database based on the ERD:

    1. to create a database for blog:
        Syntax
            CREATE DATABASE blog_db;
    2. to create a table for users:
        Syntax
            CREATE TABLE users(
                id INT NOT NULL AUTO_INCREMENT,
                email VARCHAR(100) NOT NULL,
                password VARCHAR(300) NOT NULL,
                datetime_created DATETIME NOT NULL,
                PRIMARY KEY (id)
            );
    3. to create a table for table posts:
        Syntax
            CREATE TABLE posts (
                id INT NOT NULL AUTO_INCREMENT,
                user_id INT NOT NULL,
                title VARCHAR(500) NOT NULL,
                content VARCHAR(5000) NOT NULL,
                datetime_posted DATETIME NOT NULL,
                PRIMARY KEY (id),
                CONSTRAINT fk_post_user_id FOREIGN KEY (user_id) REFERENCES users(id) ON UPDATE CASCADE ON DELETE RESTRICT
            );
    4. to create a table for table post_comments:
        Syntax
            CREATE TABLE post_comments (
                id INT NOT NULL AUTO_INCREMENT,
                post_id INT NOT NULL,
                user_id INT NOT NULL,
                content VARCHAR(5000) NOT NULL,
                datetime_commented DATETIME NOT NULL,
                PRIMARY KEY (id),
                CONSTRAINT fk_post_comment_user_id FOREIGN KEY (user_id) REFERENCES users(id) ON UPDATE CASCADE ON DELETE RESTRICT,
                CONSTRAINT fk_post_comment_post_id FOREIGN KEY (post_id) REFERENCES posts(id) ON UPDATE CASCADE ON DELETE RESTRICT

            );
    5. to create a table for table post_likes:
        Syntax
            CREATE TABLE post_likes (
                id INT NOT NULL AUTO_INCREMENT,
                post_id INT NOT NULL,
                user_id INT NOT NULL,
                datetime_liked DATETIME NOT NULL,
                PRIMARY KEY (id),
                CONSTRAINT fk_post_like_post_id FOREIGN KEY (post_id) REFERENCES posts(id) ON UPDATE CASCADE ON DELETE RESTRICT,
                CONSTRAINT fk_post_like_user_id FOREIGN KEY (user_id) REFERENCES users(id) ON UPDATE CASCADE ON DELETE RESTRICT
            );